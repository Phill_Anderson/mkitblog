package mn.mkit.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import mn.mkit.web.repository.BlogRepository;

@Controller
@RequestMapping(value="/blog")
public class BlogController {
	
	@Autowired
	private BlogRepository blogRepository;
	
	@GetMapping("{id}")
	public String list (Map<String, Object> model, @PathVariable Integer id) {
		model.put("blog", blogRepository.findOne(id));
		return "blog/one";		
	}
						
}
