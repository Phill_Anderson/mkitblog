package mn.mkit.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Category;
import mn.mkit.web.modal.CategoryForm;
import mn.mkit.web.repository.CategoryRepository;

@Controller
@RequestMapping(value="/admin/category")
public class CategoryAdminController {
	
	@Autowired
	private CategoryRepository repo;
		
	@GetMapping
	public String main () {		
		return "admin/category";		
	}
	
	@GetMapping("list")
	public String list (Model model, Sort sort) {		
		model.addAttribute("categories", repo.findAll(sort));
		return "admin/category/list";		
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Category category = repo.findOne(id);
		repo.delete(category);
		
	}
		
	@PostMapping("save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(CategoryForm form) {
		Category category;
		if (form.getId()==null) {
			category = new Category();
			category.setOrdering(0);
		}
		else {
			category = repo.findOne(form.getId());
		}		
		category.setName(form.getName());		
		repo.save(category);		
	}
	
	@GetMapping("new")
	public String newForm (Model model) {		
		model.addAttribute("jspform", new CategoryForm());
		return "admin/category/edit";	
	}
	
	@GetMapping("{id}/edit")
	public String edit (@PathVariable Integer id, Model model) {
		Category category = repo.findOne(id);
		CategoryForm categoryForm = new CategoryForm();
		categoryForm.setId(category.getId());
		categoryForm.setName(category.getName());		
		model.addAttribute("jspform", categoryForm);
		return "admin/category/edit";	
	}
			
			
}
