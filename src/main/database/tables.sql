create database mkitblog;

use mkitblog;

create table category (
	id integer unsigned auto_increment,
	name varchar(255),	
	ordering integer unsigned,	
	primary key(id)
);

create table blog (
	id integer unsigned auto_increment,
	name varchar(255),
	description varchar(255),
	created datetime,
	text text,
	category_id integer unsigned,
	primary key(id),
	foreign key (category_id) references category(id)
);

