alter table blog add column category_id integer unsigned;
alter table blog add foreign key (category_id) references category(id);
