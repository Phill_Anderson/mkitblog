<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Мэдээ нэмэх</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editForm">
			<form:hidden path="id" />
			<div class="form-group">
				<form:label path="categoryId">Ангилал</form:label>
				<form:select path="categoryId" class="form-control">
					<form:option value="0" label="..." />
					<form:options items="${categories}" itemValue="id" itemLabel="name" />
				</form:select>
			</div>
			<div class="form-group">
				<form:label path="name">Нэр</form:label>
				<form:input path="name" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="description">Тайлбар</form:label>
				<form:input path="description" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="text">Текст</form:label>
				<form:textarea path="text" class="form-control" rows="3" />
			</div>
		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>

<script>
	var editor = CKEDITOR.replace('text');
</script>