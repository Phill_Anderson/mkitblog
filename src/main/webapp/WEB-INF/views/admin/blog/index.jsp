<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center">
	<h1>Мэдээ</h1>
	<button class="btn btn-primary" onclick="create();">Шинэ</button>
</div>

<form class="form-inline">
	<label class="sr-only" for="inlineFormInputName2">Нэр</label> <input
		type="text" class="form-control mb-2 mr-sm-2" id="filterName"
		placeholder="Нэр">
	<button type="button" class="btn btn-dark mb-2" onclick="searchList();">Хайх</button>
</form>

<div id="list"></div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<script>
	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	var loadList = function(page, name) {
		$.get("/admin/blog/list?page=" + page
				+ "&size=3&sort=created,desc&name=" + name, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");

	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/blog/new", function(data) {
			$("#edit").html(data);
		});

	}

	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/blog/" + id + "/edit", function(data) {
			$("#edit").html(data);
		})
	}

	var submitForm = function() {
		$('#text').val(editor.getData());
		$.post("/admin/blog/save", $('#editForm').serialize(), function() {
			$('#editModal').modal('hide');
			$("#edit").html('');
			loadList(0, "");
		});
	}
</script>