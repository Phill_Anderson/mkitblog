<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${blogs.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th  style="width: 1px;">id</th>
					<th>Нэр</th>
					<th>Тайлбар</th>
					<th>Ангилал</th>
					<th>Огноо</th>
					<th style="width: 1px;"></th>
				</tr>

			</thead>
			<tbody>
				<c:forEach items="${blogs.content}" var="blog">
					<tr>
						<td>${blog.id}</td>
						<td>${blog.name}</td>
						<td>${blog.desc}</td>
						<td>${blog.category.name}</td>
						<td>${blog.created}</td>
						<td style="white-space: nowrap;">

							<button class="btn btn-dark" type="button"
								onclick="editThis(${blog.id})">Засах</button>
							<button class="btn btn-danger" type="button"
								onclick="deleteThis(${blog.id})">Устгах</button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<c:if test="${not (blogs.first && blogs.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not blogs.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${blogs.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${blogs.totalPages}">
						<li
							class="page-item <c:if test="${blogs.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not blogs.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${blogs.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>


	</c:otherwise>
</c:choose>



<script>

	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}
	
	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/blog/"+id,			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList();			  	   
				  }
			});	
		}				
	}
</script>

