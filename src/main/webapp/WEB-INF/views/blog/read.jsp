<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>

<title>Мэдээ</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<link rel="stylesheet" href="/site/style.css">
</head>
<body>
	<div class="container">
		<h1>${blog.name}</h1>
		<div>
			${blog.text}
		</div>
	</div>	
</body>
</html>